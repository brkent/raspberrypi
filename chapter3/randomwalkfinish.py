import numpy as np
import matplotlib.pyplot as plt
import random
import math

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

def randomWalk2D(length):
    x,y = 0,0
    walkx,walky = [x],[y]
    for i in range(length):
        new = random.randint(1, 4)
        if new == 1:
            x += 1
        elif new == 2:
            y += 1
        elif new == 3:
            x += -1
        else:
            y += -1
        walkx.append(x)
        walky.append(y)
    return [walkx,walky]
    

N=1000
finalx=[]
finaly=[]
Ntrials = 10000
for i in range(Ntrials):
    walk=randomWalk2D(N)
    finalx.append(walk[0][-1])
    finaly.append(walk[1][-1])

dist = []
for i in range(0,len(finalx)):
    dist.append(math.sqrt(finalx[i]**2 + finaly[i]**2))

fig = plt.figure(figsize=(8,8), facecolor='white')
ax = fig.add_subplot(111)
im = ax.hist2d(finalx, finaly, bins=20)
plt.savefig('randomwalk2Danalysis.png')
plt.show()
