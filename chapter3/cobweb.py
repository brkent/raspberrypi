import numpy as np
import matplotlib.pyplot as plt

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

fig = plt.figure(figsize=(4.5,4), facecolor='white')
ax1 = fig.add_subplot(111)

iterations = 100

x = np.linspace(0, 1, 101)
alpha = 2.7
y = alpha * x * (1 - x)

ax1.plot(x,y, color='black')
ax1.plot(x,x, color='blue')

plt.xlim(0.0,1.0)
plt.ylim(0.0,1.0)

# Plot vector of path from Xn to Xn+1
Xn = 0.1
yval = 0.0
Xnvals = [Xn]
for i in range(iterations):
    Xnp1 = alpha * Xn * (1 - Xn)
    ax1.plot([Xn,Xn],[yval,Xnp1], color='red')
    ax1.plot([Xn,Xnp1], [Xnp1,Xnp1], color='red')

    yval = Xnp1
    print(i, Xn)
    Xn = Xnp1

    Xnvals.append(Xnp1)

ax1.set_xlabel('Xn')
ax1.set_ylabel('Xn+1')
ax1.set_title('Logistic Equation  alpha={!s}'.format(str(alpha)))

plt.savefig('logisticAlpha{!s}.png'.format(alpha))

plt.show()