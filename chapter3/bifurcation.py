import numpy as np
import matplotlib.pyplot as plt

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

fig = plt.figure(figsize=(7,5), facecolor='white')

iterations = 1000

# Bifurcation Curve
ax2 = fig.add_subplot(111)

lambdas = []
alphas = list(np.linspace(2.8,4.0,iterations))
for alpha in alphas:
    Xn = 0.1
    Xnvals = [Xn]
    for i in range(iterations):
        Xnp1 = alpha * Xn * (1 - Xn)
        Xn = Xnp1
        Xnvals.append(Xnp1)
    Xnvalsplot = Xnvals[len(Xnvals)/2:-1]
    ax2.scatter(np.repeat(alpha,len(Xnvalsplot)), 
                Xnvalsplot, s=0.005, color='red')

ax2.axvline(x=3.1, linewidth=1, color='black', linestyle='--')
ax2.axvline(x=3.6, linewidth=1, color='black', linestyle='--')

plt.xlim(2.8,4.0)
plt.ylim(0.0,1.0)

ax2.set_xlabel('Alpha')
ax2.set_ylabel('Xn+1')
ax2.set_title('Bifurcation Diagram')

plt.savefig('bifurcation.png')

plt.show()