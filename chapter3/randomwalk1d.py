import numpy as np
import matplotlib.pyplot as plt
import random

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

def randomWalk1D(length):
    move = [1,-1]
    movements = np.array([move[i] 
                          for i in 
                          np.random.randint(0,high=2, size=length)])
    return list(np.cumsum(movements))
    

# Create the walk
N = 100000
walk = randomWalk1D(N)

# Plot the results with matplotlib
fig = plt.figure(figsize=(10,10), facecolor='white')
ax = fig.add_subplot(111)
ax.plot(walk)

ax.set_xlim(0,N)
ax.axvline(x=0, color='grey')

ax.set_xlabel('Step')
ax.set_ylabel('Position from origin')
ax.set_title('1D Pseudorandom Walk')

plt.savefig('rwalk1D.png')

plt.show()