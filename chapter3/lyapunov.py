import numpy as np
import matplotlib.pyplot as plt
from math import log

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

fig = plt.figure(figsize=(7,5), facecolor='white')

iterations = 1000

# Lyapunov Exponent Calculation
lambdas = []
alphas = list(np.linspace(2.8,4.0,iterations))
for alpha in alphas:
    Xn = 0.1
    Xnvals = [Xn]
    lyapunovvals = []
    for i in range(iterations):
        Xnp1 = alpha * Xn * (1 - Xn)
        lyapunovvals.append(log(abs(alpha - 2 * alpha * Xn)))
        Xn = Xnp1
        Xnvals.append(Xnp1)
    lambdas.append(np.mean(lyapunovvals))

    outputstring = 'Alpha: {0:.3f}   Lambda: {1:.3f}'
    print(outputstring.format(alpha, np.mean(lyapunovvals)))

    Xnvalsplot = Xnvals[len(Xnvals)/2:-1]

# Plot Lyapunov Exponent vs. Alpha
ax3 = fig.add_subplot(111)

ax3.plot(alphas, lambdas, color='blue')
ax3.axhline(y=0.0, linewidth=1, color='grey', linestyle='--')
plt.xlim(2.8,4.0)
plt.ylim(-3.5,1.5)
ax3.set_xlabel('Alpha')
ax3.set_ylabel('Lambda')
ax3.set_title('Lyapunov Exponent')

plt.savefig('lya.png')

plt.show()