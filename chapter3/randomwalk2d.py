import numpy as np
import matplotlib.pyplot as plt
import random

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

def randomWalk2D(length):
    x,y = 0,0
    walkx,walky = [x],[y]
    for i in range(length):
        new = random.randint(1, 4)
        if new == 1:
            x += 1
        elif new == 2:
            y += 1
        elif new == 3:
            x += -1
        else:
            y += -1
        walkx.append(x)
        walky.append(y)
    return [walkx,walky]
    

N = 10000
walka = randomWalk2D(N)

fig = plt.figure(figsize=(10,8), facecolor='white')
ax = fig.add_subplot(111)

im = ax.scatter(walka[0],walka[1],marker='s',
                linewidth=0,s=15,c=range(N+1))

ax.axhline(linewidth=1, color='blue', linestyle='--')
ax.axvline(linewidth=1, color='blue', linestyle='--')
ax.axhline(y=walka[1][-1], linewidth=1, color='red', linestyle='--')
ax.axvline(x=walka[0][-1], linewidth=1, color='red', linestyle='--')
ax.axis('equal')

# Colorbar
cb = fig.colorbar(im, ax=ax)
cb.set_label('Steps')

# Set Labels
ax.set_xlabel('X position')
ax.set_ylabel('Y position')
ax.set_title('2D Random Walk')

plt.savefig('randomwalk2D.png')

plt.show()