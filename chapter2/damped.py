import numpy as np
import matplotlib.pyplot as plt

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

# Create exponentially decaying harmonic oscillator
timearr = np.arange(0.0, 5.0, 0.02)
yarr = np.exp(-timearr) * np.cos(2 * np.pi * timearr)

fig = plt.figure(figsize=(6, 5), facecolor='white')
ax = fig.add_subplot(111)
ax.plot(timearr, yarr, 'r-', markerfacecolor='red', linewidth=3)
plt.xlim(0, 5)
plt.ylim(-1, 1)
ax.grid(True)
ax.set_ylabel('Amplitude')
ax.set_xlabel('Time')
t = plt.title('Damped Harmonic Oscillator')

plt.show()
