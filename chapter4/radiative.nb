(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     27429,        747]
NotebookOptionsPosition[     26795,        725]
NotebookOutlinePosition[     27134,        740]
CellTagsIndexPosition[     27091,        737]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 StyleBox[
  RowBox[{
  "Science", " ", "and", " ", "Computing", " ", "with", " ", "Raspberry", " ",
    "Pi"}], "Section"], "\n", 
 StyleBox[
  RowBox[{"Brian", " ", 
   RowBox[{"R", ".", " ", "Kent"}]}], "Section"]}], "Input",ExpressionUUID->\
"6aab0a96-89e6-42e5-88c5-cfb3096d784a"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"eq1", "=", 
   RowBox[{
    RowBox[{"D", "[", 
     RowBox[{
      RowBox[{
       SubscriptBox["i", "\[Nu]"], "[", "\[Tau]", "]"}], ",", "\[Tau]"}], 
     "]"}], "\[Equal]", 
    RowBox[{
     RowBox[{"-", 
      RowBox[{
       SubscriptBox["i", "\[Nu]"], "[", "\[Tau]", "]"}]}], "+", 
     SubscriptBox["S", "0"]}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sol", "=", 
   RowBox[{"DSolve", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"eq1", ",", 
       RowBox[{
        RowBox[{
         SubscriptBox["i", "\[Nu]"], "[", "0", "]"}], "\[Equal]", 
        SubscriptBox["i", "0"]}]}], "}"}], ",", 
     RowBox[{
      SubscriptBox["i", "\[Nu]"], "[", "\[Tau]", "]"}], ",", "\[Tau]"}], 
    "]"}]}], ";"}]}], "Input",ExpressionUUID->"fa630826-5400-4853-9d4e-\
ac1ba7aceaa2"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      SubscriptBox["i", "\[Nu]"], "[", "\[Tau]", "]"}], "/.", "sol"}], "/.", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       SubscriptBox["i", "0"], "\[Rule]", "2"}], ",", 
      RowBox[{
       SubscriptBox["S", "0"], "\[Rule]", "4"}]}], "}"}]}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"\[Tau]", ",", "0", ",", "4"}], "}"}], ",", " ", 
   RowBox[{"FrameLabel", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{"\"\<Optical Depth tau\>\"", ",", "\"\<Intensity\>\""}], "}"}]}],
    ",", " ", 
   RowBox[{"Frame", "\[Rule]", "True"}], ",", " ", 
   RowBox[{"LabelStyle", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"FontSize", "\[Rule]", "12"}], ",", 
      RowBox[{"FontFamily", "\[Rule]", "\"\<Times\>\""}], ",", " ", ",", 
      "Black", ",", "Bold"}], "}"}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7312491507439413`*^9, 
  3.731249150838893*^9}},ExpressionUUID->"b785b43b-0525-4287-b2a9-\
e8e26d5a56c6"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwV0H1UjHkUB/Bnpmma5uX5ScmODCaFkFLJyvK7alcppVInVgxtXnbYVIZC
XsY2XlImNgmV3ryk00TNDKuxRcNiU1JOsmlGjWhK8lKpVrOPP+75ns+55557
zxVGbQ/dSCcIIpCqb+m/8W1DddfOxTXVXeMoAsNnQqyOH4Y5W4yl5uYEuF/I
yq7gR+OJ34nLLdkEBDxuLs/hS7CIeEXn2RAQET14+xQ/Gft576snZhDw0uWI
Txo/AxuCY2U5KwjIG7ficAq/CBtDvGoVOQRIWRXTj/GVeK4+7HDAPBqsWp1f
1vdBi6N7+a6+YjpwTNJI30n1WO/sFDOywwwcQ3rf1hxvwjcyj7beiWZAQt0C
nPW5Bf+jE0X9EW4O45VWbTSaDhdIAh8oHJigbveprf+hHdck5QpiPjHhrjjH
bnWPAYcc6hN1l1pA/7RFqPS3N3j5zhq7kCAW0IdKxtg5GjGzm8bN62WBp2m9
eqWiB5vcFjCSEi2hNtmj7v7M97ipuh9i+i1hcnr3UHFvH85PWtgsiWdD96Cu
7ZnrR+x+XaEfecmGImPlI/LEJ9zOHT0oWsYBUnD5vcHwGTPp9AdLrnIg9VfV
pjyHAeyfJsjL/MqBASgxbDkwiMsN5IquYC5I3LRK25ovmH/u66mTWVzQKbTH
G4TDOMDv1GsbPRdGxE3nnfaM4Cfaf+UZU3gwa4nF6KbK/7DOQfA3bxUPfAuE
m6+PH8WRJHfqXjkPEgQi+1eRJnxZvniOYxUPZveEc5oDCdhgp5kUYeSBNepj
aYcJKH6dMJQ6jgRG1fmyqdk00NZ9iVV7kbDVo7LFMoAO5wLU5g0iEkqWReX9
NUCH02enJy6TktDfPWFMZ6YZZEk7XKxzSVCt9DXL+JEB+++N2r64RcIBP9kF
ZScDai75xp1tJEH40v95ocwcPh7wrrIwkuA1PzmD78YED0+s2kMgiJxUP0Rr
ZIKvRebmh9YI+h+ltcdKLcBW7J41djqCLMY6SagDC+rKa92uzEdg3aYqoGtZ
wPOpCy5bisB8nzRsQZwllEyZrVaHIyCK15W1j2XDmi+V+wujEMSfK0sbe5cN
9i0Fc1K2I7g96yjLKoYDuz4eEjrvRWDlHz/7EMmFma5VP7nIELQ6P2ncreHC
xaA/b7jIEVzLxQ6GdTzoOOaS+fAMgrVlnlaCUR7EMyXC0xcQHJTlS1uvkpAi
L7y14TKC0reC8+RyBP5BE/S2CgRHc2Tr24IQMB2Ki9wpR4e+c1SEUHsSBaJg
yhM1mmuBEQgWPdGqjlFOka+5n0rdrfaOM45Q3uJ59gN7NwJWWrikrQyBfbKN
n8Ulqm8stc++jmD0+yRe8xUE4Vrl1RuUW951PL1UgsDPb4xTI+X0iIq1S6mM
7njKYpcjMM0M3SHTIPh97uuwBMqtDfJcs0YEnfmu9sEVCG4eHvyl8Rn1F854
q62UMxaKnAqfI5jSeWRARjng4hyldxuCQi/bi5WUbyU+fijtQmDH9z45TYkg
03mePKQHwbZnzz2WUI5rzw4TvkfQvMumfg3lGcu36ao/IziReuRNOmUGrako
fRDBHfm9jSWU9aqF4vXDVJ5WvbhHWSMudHH9imBvx89L9ZTPTOb0m0zUnObm
lWHK/wMoXxPA
       "]]},
     Annotation[#, "Charting`Private`Tag$5740#1"]& ]}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 2.0000001632652995`},
  DisplayFunction->Identity,
  Frame->{{True, True}, {True, True}},
  FrameLabel->{{
     FormBox["\"Intensity\"", TraditionalForm], None}, {
     FormBox["\"Optical Depth tau\"", TraditionalForm], None}},
  FrameTicks->FrontEndValueCache[{{Automatic, 
      Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
      Charting`ScaledFrameTicks[{Identity, Identity}]}}, {{Automatic, {{2., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.5, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.5, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.5, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.7, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.9, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.1, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.3, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.7, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.9, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.1, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.3, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.7, 
        FormBox[
         InterpretationBox[
          StyleBox[
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.9, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.1, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.3, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.5, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}}}, {Automatic, {{0., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.01, 
        0.}, {
         AbsoluteThickness[0.1]}}, {-1., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {-0.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {-0.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {-0.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {-0.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {0.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {0.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {0.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {0.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {1.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {2.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {3.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.2, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.4, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.6, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {4.8, 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}, {5., 
        FormBox[
         InterpretationBox[
          StyleBox[
           
           GraphicsBox[{}, ImageSize -> {0., 0.}, BaselinePosition -> 
            Baseline], "CacheGraphics" -> False], 
          Spacer[{0., 0.}], Selectable -> False], TraditionalForm], {0.005, 
        0.}, {
         AbsoluteThickness[0.1]}}}}}],
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  LabelStyle->{FontSize -> 12, FontFamily -> "Times", Null, 
    GrayLevel[0], Bold},
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0, 4}, {2.0000001632652995`, 3.963368719232223}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.731249145579398*^9, 
  3.731249153756662*^9}},ExpressionUUID->"1cc275a3-2beb-415f-a0b3-\
1ddb3dfdf0cf"]
}, Open  ]]
},
WindowSize->{1398, 1691},
WindowMargins->{{987, Automatic}, {158, Automatic}},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (April 18, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 300, 8, 79, "Input", "ExpressionUUID" -> \
"6aab0a96-89e6-42e5-88c5-cfb3096d784a"],
Cell[861, 30, 833, 27, 59, "Input", "ExpressionUUID" -> \
"fa630826-5400-4853-9d4e-ac1ba7aceaa2"],
Cell[CellGroupData[{
Cell[1719, 61, 1012, 28, 59, "Input", "ExpressionUUID" -> \
"b785b43b-0525-4287-b2a9-e8e26d5a56c6"],
Cell[2734, 91, 24045, 631, 253, "Output", "ExpressionUUID" -> \
"1cc275a3-2beb-415f-a0b3-1ddb3dfdf0cf"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

