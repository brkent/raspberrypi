(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     25708,        576]
NotebookOptionsPosition[     24697,        544]
NotebookOutlinePosition[     25035,        559]
CellTagsIndexPosition[     24992,        556]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 StyleBox[
  RowBox[{
  "Science", " ", "and", " ", "Computing", " ", "with", " ", "Raspberry", " ",
    "Pi"}], "Section"], "\n", 
 StyleBox[
  RowBox[{"Brian", " ", 
   RowBox[{"R", ".", " ", "Kent"}]}], "Section"]}], "Input",ExpressionUUID->\
"ea5b0ccb-62f1-4c2d-a73a-bb782a62c9ea"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"distribution", "=", 
   RowBox[{"MixtureDistribution", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1", ",", "0.5"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"MultinormalDistribution", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"10", ",", 
           RowBox[{"-", "2"}]}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"2", ",", "0"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"0", ",", "2"}], "}"}]}], "}"}]}], "]"}], ",", 
       RowBox[{"MultinormalDistribution", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
           RowBox[{"-", "7"}], ",", "1"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"2", ",", "0"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"0", ",", "2"}], "}"}]}], "}"}]}], "]"}], ",", 
       RowBox[{"MultinormalDistribution", "[", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{"0", ",", "8"}], "}"}], ",", 
         RowBox[{"{", 
          RowBox[{
           RowBox[{"{", 
            RowBox[{"2", ",", "0"}], "}"}], ",", 
           RowBox[{"{", 
            RowBox[{"0", ",", "2"}], "}"}]}], "}"}]}], "]"}]}], "}"}]}], 
    "]"}]}], ";"}], "\n", 
 RowBox[{
  RowBox[{"data", "=", 
   RowBox[{"RandomVariate", "[", 
    RowBox[{"distribution", ",", "200"}], "]"}]}], ";"}], "\n", 
 RowBox[{"ListPlot", "[", 
  RowBox[{"data", ",", 
   RowBox[{"PlotRange", "\[Rule]", "All"}], ",", " ", 
   RowBox[{"Frame", "\[Rule]", "True"}]}], "]"}]}], "Input",
 CellChangeTimes->{{3.7312532643989553`*^9, 
  3.731253277917716*^9}},ExpressionUUID->"aa52163b-4dc9-4bdd-8811-\
38f1a07c3ede"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.009166666666666668], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJwVlmk4FFwDhif7HoXMmDGbQSiyhzpH9iWVSpKURKVC6rWFt0irvXi1qVRM
EcpedE6RLSKEQjH2Jfsysn19P57r/vH8va/reejHfBw8eAgEQtXf/J88eh2L
XoCAf174MUPnKsCKpxT9vSEsWNPm0v5u1wAaHKnpeexLx2WWPaTDg/xwVW8s
JugaAd59sNwdbagAra/8TtYZZcGuprDEuKA5VJ+/kCHwhIqN1Th+VUOVYOP1
ff5PKVMgoUhI9GEJFQb4dazlPmfCgeUlpGImhqce2b826mNBTrTyDZIHAQ8u
vN92QEQJ7uu0o5bJ8ODupF25XiPyOE6TJNd0pAwRX4ssnTxDxxyT/UU7D/PC
oBPzI03f5bBehTXB7Rk/dBoyo2vnS+Gf5/zuvLjMRVONH8JP6jEhc3ft19mA
GcRoD3N01GLBgZKLF174L6L/lAqHvNg03NARZdZUOo2ci9kZqRIsKNh3dG+P
xQKKDyhRr6eS8Ufe8wEu+b/AltLT1gbSTBir2cTn2yGIwzVUE8R7abCW79fG
zRd5cTRp736OEQ2LWfwz58FpRgdXUKhZMT/+0Z97Rf04BV699jItnU3EfLaf
U9jLBDgaIdGXl07BqvC78xMiF5zSYF/SPsWC9lW/h3K1BfG9nWKUs85MuMTd
rhgd8gsV8MbWtunww1tcqVaOKwPOX9NxldElYpJcXJrejkYgFeGjSKSJ4Yor
5EdfmhcBT6fo0g4LKt6Wo/zWNHAepErO93IeUbHB/QWJGv4qkFOe+z7+2jLy
7/A5YnJMAVZafb1ms00ByxXEWrzJzQNCCRT5Ui8m3C16V3frtVXEOSt70kuN
Af2/35sK0OhDdms2h1WJKuLUpc/eafFzwLKLLrU5bB1mT5AeldtMo/ZbK4yV
DDq2uhw0/gBXIJvxEcEEPyY2pP/RLxkeBlWDI2HxlUpw+a33sQi7NfhNvXJ5
rx4VRv34cHHJThif8w616ZIkYsP7Fj2Orr1IOsro8sbPLChEuC23L2IW4Us/
rnfW0WFVj9drqo84XvPZdFKvVxLnabwKYYTPoF3rGYdVWJ+Acd3rnFoJCjQy
05eQVVSGowfqVq+f7Qb6YMvqUXcluDnk158Q8jC6s2b4T9YDGswO2j1pucyP
X52Y+1XzeBq495kIGLcw4YRmQBKVzYJFt+2zW13a0YeIETOZ8W/oku3LtthB
KiQ4prKDb5KxAe1sZXZQFQKjmzIyrJhQbsxn1OsCF0228l0471IJ2FEDROFY
Geh0OHm9kzsXyMiOHmT/qwiDWaPktkAW9lx4IV4iuAKMS8dQ9VY6NLd7VPOt
XAA75N72E3s9gXIitbkZaxWh0VuPHf/pkXFVsknvvFYdmp197iIZQMFdTttJ
JWZ8sHjauX/yfT9wOv+VU9DBgC+zqYyQdiW4uKClWxzMg1992H5D7wAJdxEa
Xxz3fQeQK6ck7j0dyp6QoXjASfSmmzLYssIBpzMmGq1LaXA7//blL8IMeK/B
9iG7UASfF9ZJ8j0ui9MeB1zYmcED1/0M45KSyLiB1BIrmvUcOInQt/4Ul8Z2
v2Nma4WbUWFODMX0NxXvA8zDzZX88And1mHbb0XIW3lzNu79Eoqbj672laTj
xLNd5Jnfg4DpRLH6SFeE4QGnbus+4sNH59sa0tQQSOfE8fSyFGBWOTrYL68M
BYgxTqYqkjhIY8FL/7gy9E2XUG8O6UawTWzT9/vKkD00vSHz0yrK03n7ID2G
CXdUGEhxr/Bj5zH7No4OE8akJxzh1PNg9Rnpf0d1ZXGDjWVKydevSGo46/4R
NwaU3PCHcsdlBCm26yb8aafhwVVmd5NPL2CQsZPPZQb0HrC2yWNPouG33rPZ
GiyYMz6THeJOwkNbT4WsfUDCp0Jzap5PjQH2TOv4bBAJ539aZVU58kILa1K2
iAMdP1vx3uM/xwUGA9aNbjYjIHhgz8xPdxq8vT+BX3LfCmDrq6fc38eAxPGo
JGaeImQ1Kmb3AUEsbUGZ+H6TAdkfX8SHLQwgLefqnwGh0riojubkvNKGJEIL
y7kdVFzdbzNy+WkX6Btfhu1URXg17krdyPgAGpWMTAzOUYKe4eamTdqSuO5Y
qrDlpk5EK+6Cts/loWI2R8LiggLeUULQS19oBLumY9ZWF0hjydYuo8mnCIT7
pBJ7jcvReq2DaVFxNBg5mtn1vE4O02xTH+S8HUYTdpt8vW9RYIGbiZ6QvxAu
WBI1V02Rw/rvPa4khowDTx21dzIlBOgbdENPjS4PH6euNm6dU4DGrAC3GglR
XFnGBjnyslhi3Hrgwb+S8JDRySElbzmcNbe65jCZB9o5VW4oG6VhdVbPPzG6
3Ujeu9xSYpkBO3m8T/xBknhnkPBi4R0iPuopNfVq1xByOHjgJPk9Ex46bimQ
UySB9wu+nEgPpGErHsdBE04tyh8qNTkzWQE+vGak2wfQ4WLhqwfB2etx1/ja
vdK8y+D8qXTHV30MHPfgmWzFXino367Ag2zpcLNAscNE9ALKFGG4JNs3griH
eufbNOgws4nFwGpM2Oz0uJAgOYi8rrKPfP6HDveN8jzkc1tFWYui/uFpypBw
0uuGcOoiylU1ErjVSsEpvPxsgww+eDHmhNdEIRP36w5ZWMuvgYxmstsaTxU4
1eXb4GsmgkWWB4uj9jKh4N2kRlPSDNra2SMFHKdAnXq5q0cJEyIQ5K289AUA
IU9Fh1wFWCdNna2XIeKoihSKXD8XFXj7WvkIyGI9/u6TxSviMOHYK0//CGmc
6XE4IcZPAKr1dhfkEpSganqTzLY9I8joS6kZr50C/rOobV7KXAJPOjKT6qyU
YctCbKJhmCBW0TQvDOphYs1aAQeZ5C8o2pimWRtExnhdMbqr0YR+3MqP8rVS
hHmv58LH4haQZKFyyWI1B6iePuTA2kOCW0brI6ae0vHw8UpLk6RJFN9yyuZ5
FREHKVYVeJjzws2uJyx1bKSxj2b3s9jH/HCXL7FDa30XiFAfQUL+ZOiXlqJS
f1AO0javy/c2bQc8x3MqdilNAGsPs4H9L4hwaGrvxgJ1Gjx0+td0aIsAtnDr
HnLZSccV1UelU8RawFBFjRFRRgmaX5StNUskYLWr4wLR6zfglEpzFfUtPeCt
9H63b5p02OPXTdS/z4vL+e5+yz/DgmZnrc95sMSwu/bYxLA8E+amR28M7xPE
xPjRq5c8yLioyUSev74bOYpfol6+wYSaByYi77K5qDYi9GPmBQo2uFAum7/y
FV0zo/QRPSnY+e6VZJ28ZpC52fCwcesXdPCzY5/9NSq0nN6RkGpHw+Z+qwmm
dXPguuf35ZqPyyBL8AoxSJUBiz8u2P7nQcKh8lJFqT+XQCQzRmvtYCfIHNRN
uWkvA41TNbNu69CwEUPr6a6qUWCXZ6umk6gM9UMiS01OCuJn932L1obSYbCc
btF40SD6TgMGJL9pkHSs9cCrTDq0d0vwEaTNosEe8XfTZCa8F7g3EsoxIQ9F
7LFEwTo8ed10t1nUIvKZMgx/8revDx0T6LARwg1adxyP59Pgm9zATbUxZKy1
yfnbx4FfQHXp3kSZKR3S3ojvjIzsA8GRcmcue9LhktgpddkRcWx+rtVN760M
3tnBnhdZmQZmb/ZsNy8hYrffglSrkGWA9q/5IuPAB11dgwOBBBWmmqu2WX9Q
gVzJfICzWgG/xfz6S9ny+NLOmk9GFgSI/VVxlpoC3k4uKLB3H0TM3WMT63XJ
2Cg+QmRL8zBw7BY8llg/D5z6iQ6eZX//bVeyQn0HF92SZY9xY2iQh1vo6/R3
/5+9JDhLnVuLaVTN8nFAgxruBspHW/hwQyeRKqKhBMMm5upxGgdNehkGbjq/
FZ176L0hc4EBXXNOMDS+KkLVgaji+HBhHPv55rrgvz7wWLwb7ssj4PlDDRq7
bMnYUC5lnlvFC69ra4vZJ9Px0bIbU8o3ZwBPcpO7yj0KlnKpniH8mAC/f7DK
IjKrkEPx1YcHhWRgAt/wJ/pjWezf2wZlK34Dd9EhNbIUDevIGhco+a2AvjK1
2pQPFMzlCH3Wra4FnY+D2tyG25DhWUpzitlfvx7WfapJ7EXHDKL4Y45Iwiyx
9ed6ZYnYYI+AfeyWFXDzrt8msUIKFpjgi3jWLgSnY2cDAweHELGlW8RhlAyD
vXZ0pg+z4NFYhU8Hmt6DF2FSmrcD+xDhdJ5Q6XMG1JfW6mcM/EBySSuXO2kk
+D/47wXZ
      "]]}, {}}, {}, {}, {{}, {}}, {{}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{True, True}, {True, True}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{-10.736023811174835`, 
   12.377516513023526`}, {-6.597155331921314, 10.249331293172}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.731253268897004*^9, 
  3.731253278898395*^9}},ExpressionUUID->"750f0683-b120-4601-9c4b-\
aebdcc461329"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"cl", "=", 
  RowBox[{"ClusterClassify", "[", "data", "]"}]}]], "Input",
 CellChangeTimes->{{3.73125328753498*^9, 
  3.7312532875354347`*^9}},ExpressionUUID->"af7540c1-7dbb-4a43-9ef6-\
fa10e64e8b63"],

Cell[BoxData[
 TagBox[
  TemplateBox[{RowBox[{
      StyleBox[
       TagBox["ClassifierFunction", "SummaryHead"], 
       "NonInterpretableSummary"], 
      StyleBox["[", "NonInterpretableSummary"], 
      DynamicModuleBox[{Typeset`open$$ = False}, 
       PanelBox[
        PaneSelectorBox[{False -> GridBox[{{
              PaneBox[
               ButtonBox[
                DynamicBox[
                 FEPrivate`FrontEndResource[
                 "FEBitmaps", "SquarePlusIconMedium"]], 
                ButtonFunction :> (Typeset`open$$ = True), Appearance -> None,
                 Evaluator -> Automatic, Method -> "Preemptive"], 
               Alignment -> {Center, Center}, ImageSize -> 
               Dynamic[{
                 Automatic, 3.5 CurrentValue["FontCapHeight"]/
                  AbsoluteCurrentValue[Magnification]}]], 
              GraphicsBox[{{
                 PointSize[0.13], 
                 GrayLevel[0.45], 
                 
                 PointBox[{{0.9821769431797024, -0.440194219686987}, {
                  1.1339776261519132`, 0.8056918676854272}, {
                  0.5279892326667741, 0.6574306661126254}, {
                  0.022147046479890797`, 1.4937877187998898`}}], 
                 GrayLevel[0.7], 
                 
                 PointBox[{{-0.9815166384819979, 
                  0.15045697525228735`}, {-0.5923526886966953, \
-0.33441771553094035`}, {-0.005656646679640442, -1.462421365651345}, \
{-1.0734370436522753`, -1.3729645043477454`}}]}, {
                 GrayLevel[0.55], 
                 AbsoluteThickness[1.5], 
                 LineBox[{{-1., 1.5}, {1, -1.6}}]}}, {
               Axes -> {False, False}, AxesLabel -> {None, None}, 
                AxesOrigin -> {0, 0}, BaseStyle -> {FontFamily -> "Arial", 
                  AbsoluteThickness[1.5]}, DisplayFunction -> Identity, 
                Frame -> {{True, True}, {True, True}}, 
                FrameLabel -> {{None, None}, {None, None}}, FrameStyle -> 
                Directive[
                  Thickness[Tiny], 
                  GrayLevel[0.7]], FrameTicks -> {{None, None}, {None, None}},
                 GridLines -> {None, None}, 
                LabelStyle -> {FontFamily -> "Arial"}, 
                Method -> {"ScalingFunctions" -> None}, 
                PlotRange -> {{-1., 1}, {-1.3, 1.1}}, PlotRangeClipping -> 
                True, PlotRangePadding -> {{0.7, 0.7}, {0.7, 0.7}}, 
                Ticks -> {None, None}}, Axes -> False, AspectRatio -> 1, 
               ImageSize -> 
               Dynamic[{
                 Automatic, 3.5 CurrentValue["FontCapHeight"]/
                  AbsoluteCurrentValue[Magnification]}], Frame -> True, 
               FrameTicks -> None, FrameStyle -> Directive[
                 Opacity[0.5], 
                 Thickness[Tiny], 
                 RGBColor[0.368417, 0.506779, 0.709798]], Background -> 
               GrayLevel[0.94]], 
              GridBox[{{
                 RowBox[{
                   TagBox["\"Input type: \"", "SummaryItemAnnotation"], 
                   "\[InvisibleSpace]", 
                   TagBox[
                    TemplateBox[{"\"NumericalVector\"", 
                    StyleBox[
                    
                    TemplateBox[{"\" (length: \"", "2", "\")\""}, 
                    "RowDefault"], 
                    GrayLevel[0.5], StripOnInput -> False]}, "RowDefault"], 
                    "SummaryItem"]}]}, {
                 RowBox[{
                   TagBox["\"Classes: \"", "SummaryItemAnnotation"], 
                   "\[InvisibleSpace]", 
                   TagBox[
                    
                    TemplateBox[{",", "\",\"", "1", "2", "3"}, 
                    "RowWithSeparators"], "SummaryItem"]}]}}, 
               GridBoxAlignment -> {
                "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
               False, GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
               GridBoxSpacings -> {
                "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
               BaseStyle -> {
                ShowStringCharacters -> False, NumberMarks -> False, 
                 PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
            GridBoxAlignment -> {"Rows" -> {{Top}}}, AutoDelete -> False, 
            GridBoxItemSize -> {
             "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
            BaselinePosition -> {1, 1}], True -> GridBox[{{
              PaneBox[
               ButtonBox[
                DynamicBox[
                 FEPrivate`FrontEndResource[
                 "FEBitmaps", "SquareMinusIconMedium"]], 
                ButtonFunction :> (Typeset`open$$ = False), Appearance -> 
                None, Evaluator -> Automatic, Method -> "Preemptive"], 
               Alignment -> {Center, Center}, ImageSize -> 
               Dynamic[{
                 Automatic, 3.5 CurrentValue["FontCapHeight"]/
                  AbsoluteCurrentValue[Magnification]}]], 
              GraphicsBox[{{
                 PointSize[0.13], 
                 GrayLevel[0.45], 
                 
                 PointBox[{{0.9821769431797024, -0.440194219686987}, {
                  1.1339776261519132`, 0.8056918676854272}, {
                  0.5279892326667741, 0.6574306661126254}, {
                  0.022147046479890797`, 1.4937877187998898`}}], 
                 GrayLevel[0.7], 
                 
                 PointBox[{{-0.9815166384819979, 
                  0.15045697525228735`}, {-0.5923526886966953, \
-0.33441771553094035`}, {-0.005656646679640442, -1.462421365651345}, \
{-1.0734370436522753`, -1.3729645043477454`}}]}, {
                 GrayLevel[0.55], 
                 AbsoluteThickness[1.5], 
                 LineBox[{{-1., 1.5}, {1, -1.6}}]}}, {
               Axes -> {False, False}, AxesLabel -> {None, None}, 
                AxesOrigin -> {0, 0}, BaseStyle -> {FontFamily -> "Arial", 
                  AbsoluteThickness[1.5]}, DisplayFunction -> Identity, 
                Frame -> {{True, True}, {True, True}}, 
                FrameLabel -> {{None, None}, {None, None}}, FrameStyle -> 
                Directive[
                  Thickness[Tiny], 
                  GrayLevel[0.7]], FrameTicks -> {{None, None}, {None, None}},
                 GridLines -> {None, None}, 
                LabelStyle -> {FontFamily -> "Arial"}, 
                Method -> {"ScalingFunctions" -> None}, 
                PlotRange -> {{-1., 1}, {-1.3, 1.1}}, PlotRangeClipping -> 
                True, PlotRangePadding -> {{0.7, 0.7}, {0.7, 0.7}}, 
                Ticks -> {None, None}}, Axes -> False, AspectRatio -> 1, 
               ImageSize -> 
               Dynamic[{
                 Automatic, 3.5 CurrentValue["FontCapHeight"]/
                  AbsoluteCurrentValue[Magnification]}], Frame -> True, 
               FrameTicks -> None, FrameStyle -> Directive[
                 Opacity[0.5], 
                 Thickness[Tiny], 
                 RGBColor[0.368417, 0.506779, 0.709798]], Background -> 
               GrayLevel[0.94]], 
              GridBox[{{
                 RowBox[{
                   TagBox["\"Input type: \"", "SummaryItemAnnotation"], 
                   "\[InvisibleSpace]", 
                   TagBox[
                    TemplateBox[{"\"NumericalVector\"", 
                    StyleBox[
                    
                    TemplateBox[{"\" (length: \"", "2", "\")\""}, 
                    "RowDefault"], 
                    GrayLevel[0.5], StripOnInput -> False]}, "RowDefault"], 
                    "SummaryItem"]}]}, {
                 RowBox[{
                   TagBox["\"Classes: \"", "SummaryItemAnnotation"], 
                   "\[InvisibleSpace]", 
                   TagBox[
                    
                    TemplateBox[{",", "\",\"", "1", "2", "3"}, 
                    "RowWithSeparators"], "SummaryItem"]}]}, {
                 RowBox[{
                   TagBox["\"Method: \"", "SummaryItemAnnotation"], 
                   "\[InvisibleSpace]", 
                   TagBox["\"GaussianMixture\"", "SummaryItem"]}]}, {
                 RowBox[{
                   TagBox[
                   "\"Number of training examples: \"", 
                    "SummaryItemAnnotation"], "\[InvisibleSpace]", 
                   TagBox["200", "SummaryItem"]}]}}, 
               GridBoxAlignment -> {
                "Columns" -> {{Left}}, "Rows" -> {{Automatic}}}, AutoDelete -> 
               False, GridBoxItemSize -> {
                "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
               GridBoxSpacings -> {
                "Columns" -> {{2}}, "Rows" -> {{Automatic}}}, 
               BaseStyle -> {
                ShowStringCharacters -> False, NumberMarks -> False, 
                 PrintPrecision -> 3, ShowSyntaxStyles -> False}]}}, 
            GridBoxAlignment -> {"Rows" -> {{Top}}}, AutoDelete -> False, 
            GridBoxItemSize -> {
             "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
            BaselinePosition -> {1, 1}]}, 
         Dynamic[Typeset`open$$], ImageSize -> Automatic], BaselinePosition -> 
        Baseline], DynamicModuleValues :> {}], 
      StyleBox["]", "NonInterpretableSummary"]}]},
   "CopyTag",
   DisplayFunction->(#& ),
   InterpretationFunction->("ClassifierFunction[\[Ellipsis]]"& )],
  False,
  Editable->False,
  SelectWithContents->True,
  Selectable->False]], "Output",
 CellChangeTimes->{
  3.731253289232254*^9},ExpressionUUID->"0434e3a9-6302-4550-8974-\
a1c620a13c52"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ListPlot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Pick", "[", 
      RowBox[{"data", ",", 
       RowBox[{"cl", "[", "data", "]"}], ",", "#"}], "]"}], "&"}], "/@", 
    RowBox[{"{", 
     RowBox[{"1", ",", "2", ",", "3"}], "}"}]}], ",", 
   RowBox[{"Frame", "\[Rule]", "True"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.731253295384012*^9, 
  3.731253295385089*^9}},ExpressionUUID->"9008b103-00bc-4dc2-b7fc-\
49805baba184"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.009166666666666668], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJwBQQW++iFib1JlAgAAAFMAAAACAAAA6OvK46BwIcDDR+MaXegFQM+1+asG
oR/APCvicsjpxz/hlk+zausbwJAwGhjTX8O/GbEK/GhsIcDiQVW4TV0DQHln
9+vT2RjANsZJAGGkBUBY6UQhM7URwN5xcpSpgfi/lya36WqoIMDQ3IxE073z
v5F3vCzPHxzAwgNzd1y13z+NGlNV4jsgwAxGdfZl4tS/hoeqpqeoGcAES8ue
qP0AQOyDDuW0px3AKkDZWqEZ+D/3hzReFTUZwBoYkKY2QtI/EYNvJBkgDMDG
hByfztT7PwLdC/xCRh/APbAnukN49z+iEPfk4p8fwDib+Q7KBcg/x0jRh0o9
HsAYto9GsrO0P0wBLn7ICyTAovzLbqaR9j9H4CERLn4SwKjwGp/ESvO/24v+
Iv6rIcBIgXnvnMDGv0rv6weSciPAOiH6N7zq6j9xbn1K4BAZwDqbRuNWXuS/
ActD8TbkEMC0L618IoL0vwBWoqh6ihzAOCBtx695yL96Jewc2HglwGb5qQ28
B/4/O7plQpc2HMDImEHk9zLNv/X1pVwQdx3A4Fg+GrxEBECtwT6JNlcawOAA
0qlkcLs/cwk0lnBkFsCmoHd0TasCQBLefvgalhzA0BrWjwuupT9YCiE53g0U
wEztjvXMCdS/t7COHUPtH8BUPyNd1McFQJD3jclwECHAlW3gHPTt6D8s9BR/
7DUWwNBKR5680dG/JNs1kvrbIMDo/yPh02/kP+k5aXwPnBrAaX2wyqXy7j+o
9Nfv9XkawLXF/yXIVgNARkkarwpSIcCk/m5Rdvb4PzJayd53fRTAuM0gWFr+
2L8OfbfE+NwfwMnmSuuBo+A/JK/iDkZ0HsBCvAA2p/nSP0/zjg/JthTAENfg
O/Gjvz+F7Kzgpc0YwCBLopywuuq/tvwLRSqeGMA3vmWElXzvP8fDqD+wGxbA
Du9J55x/EEBbO2jpJm4YwK72/wFdHAJATFjHF8PsIMAsJeN1jjXhv015Cfu3
lBnAYGYR8q1P6b9VB6rwp3ggwEgCVuhB4sy/+7etnHqvE8Dg7w9TFAP9P3Np
p1at5SLAkJykFsZTEUCzKjsGi9cdwJ4DBag4qwRAe45navC3I8DmNelGSRsB
QM0UH/XPFRnAjMaeHRjm+L+2bnBIbwYWwDYF4Wi5/g1AkmKtZnaDFMCsZV2S
jnIGQDvOvUQDTB7A+vszRb0j/D8oMEW3eeMjwDDMBlIVmM6/jTwgMMx5HMDA
Erm/mS/TvzHsz4PyoyHA6mTHR0GW8b+R1mlKpcgZwHkkyLZlRQNALl5nRzRK
FMBvMOGkj6AFQEZh4elcTSHAxslgFJ4M1j8rhu8GjRMXwJ7HRSgsMeM/GZHs
hoBlHMC400EbBc/hv8yDfcKsdB3AOHTEFrX+0b+HRB3lGWYdwFqZhJg0tNQ/
R/NCkqJMIMBFcv+SQ832P7nC+UuXZRrAfRsRuKLe/D88ojCukzQgwDsiMqNP
yOw/srN4LcyOHMAyLVrVwuffP0Vx12E2uhXATdyo9wr+8z9EslE+RbwZwGHt
Bx9IfP0/BUb3E4CvG8CATcrFO0YAQMB2KsCuKx7APhy2tk5j6L8jUO7wEzUc
wDuRgwox1Oo/91vQL09LHMA6GJ73+MgDQIgzMwxOmCHAYMOJ8ieK9D8CmNNj
KJodwBFcyfQA2vA/kgTqxSGgFsB25NhAFsbtP2ML6SscESDANBY8tiZy/j/l
wyvMnsEdwPjiCMs1ycw/rgwTceQWGcA4UQZOjzH+P4qZci0Mtx3ABvAEg6Tb
CEAKXZJ9
      "]]}, 
    {RGBColor[0.880722, 0.611041, 0.142051], PointSize[0.009166666666666668], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJwBoQRe+yFib1JlAgAAAEkAAAACAAAAxqMdN1N8JUDK2Fzbu0/nv0mE7Zg0
7CVA4NN+lZB59r93ctwP+KUjQOf9/L8oRAzA8p9OsTvlJUDijSeJGmUAwOj5
vj1XCiZAVN1MH8MVAsDy0sGCaDYjQCNQzNH1d/S/Itt+VlYyJUDnvHt0qXb7
v1q5qKuiDiVAB+VgU+NG+b8xvWtJOBQjQI8w0wRw3AfAgi8qkg3kIEDMBN8p
LnsDwGkvqIAzaSVATsjt6bMzB8CaTQwdbVojQPz4PiSNfN+/CJIdG71qI0BQ
C5k1OYf/v+JtFmhqKyJAdtma8ncv5b/I6Ot+kccmQP26bmKDTAHAss8nxOQ2
H0CM2sF7/EwJwBSMO4EpyyVACACTGFSD9b/AgNqI3c0hQMjjarEfbw3AO0Q3
DhYkJ0DsV83/iG3hPzc/Mf9gYyZALnzf+nwc6r+UAer6rpwgQK95UPFH/QXA
8DB3lh+oJUC4k06v11zbvz/sLaurSCNAGO5v7Gp0+L88ve6/yTkhQEVMn8rV
xAbAqq8fInzbJkD7+TI1uXoCwL9e4ryQviFAFmcVHWVA8b8+BT79zgkiQJrQ
S52otwrAoSFLUj3tJEADx4r1kL78vyNYHUjCISRAgndpkzWfBMCuxL9Z5hsn
QAYZjlhDKBDAeS/5ajdkJ0Bwpw4s1Hzhv0DYDC3ZmydAqOnzF6zF/7+0NLqc
p44jQELGOBH4hAXAWu5O2OI0I0COp5Jf4s8CwBHqrptfYSJAEBf6HZRc678i
HMBYb4EiQG7nSUq0qPG/6rpu9a8vJUCw7/SvfGMawBnvjJYjtCRAJdIkr+U/
B8AURh3w2YoiQKjCqZF++ee/5e/9QNsfJECGkITN6+/nv+wQhZV6sCZAZoJF
Q9MzEMDwTC1wbosdQLZhQTYIdgjAoKL/0jn2HkA8JXdhyg4LwBtuxEcO/SJA
3QJuZ/q/EMBSWVdoHL4jQFtkRwawuA7AdtseAr9LIUAuBrlS8I35v6zTJSLA
KyNA1FigtwAQ6L9qhqhfy3UhQFTsAp0EYf+/rvsLdoKmJ0AAaGqJCaL7vyLU
HGEBZihA8uBw0HBECsAK/ei5jFMjQAeZltJDGvS/K+ThtrMAJkAqp9MVPVHr
v6HcrJbNSCdA1vmPlTp+B8Dai7WMcEgkQLSx9oLukPm/cqaeKM9ZGEAgLhK1
bkPbP+nyUym2LCBAW2vf833WBsDpxso7GRUmQEV7FsxElQDAuhRVYdUwIUDj
cuEZN5sDwMQEmdW1bCVARG1JcWUlDMBjM+7w6hsjQLOnjSmC5QfAVg2AH4GJ
I0AwV/CFmaj4v0y0Sys0lSdAN3yFvUFoB8Ckm3C4D30hQHoYNbjvuOi/mnhT
hUAYI0ACHQygDrYSwCr8mvDDQyFAILINTYWF5T96hRhsgWYhQPwMaSwW6w3A
okUq2EnBKED4ELU/wK7XPwL4t3BYqCVApKoAWhFxD8AgHzDE7z8gQC9jOCdg
1gTA0N0ZHwovJkB+8PbPwKbiv16wZyIv0SRAKueMuZGCCcCPy4oSeokjQAJG
u+rltADAempC3afqJUBgjx7FV9O+P1dnKFo=
      "]]}, 
    {RGBColor[0.560181, 0.691569, 0.194885], PointSize[0.009166666666666668], 
     AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJwB0QIu/SFib1JlAgAAACwAAAACAAAAAjbc+2o/AMDedNr0IfgeQP827o55
hwBAmZz94Y06HkApiFR2ox3yP5K4CAudvB9AWf6/fUS5BcDa5rOELGQdQLYD
j8zYNAVAi/gR1+JeIkCwxLO+kYf9v3bcb19BYh5ATxMiXSglxT88zbGwzA4d
QK1n9t/KoPM/Y+VBBjzWI0DBg+tEFe/Vv4BLqtiP6B9A8dcEdHNcxz+ojOcZ
CY8VQFhdmBNYY/g/FRbsWah/JEBSs5NyDLHwv7CFM/irDyRAufNa5vG+5j9Y
c9HittwiQLLhHejW/uI/a6vw0km9IEBg99jQpiu/P6fikALkJR5AOOdJ0mFK
6z9651H03mMgQJNVkgUQVP4/qDcsnptUIkDNYqIJRy3dvyC54EBLpRtAgm+i
GeQ8xL8TMlmmjJAgQGY0K7sVvABAcHmJNishG0C16b1BbPHGP8GxIqdOdyFA
rAoiXJhO0j+QnTZz2C8hQDnd4xE/VvI/zSzEXmW8I0C/P3luJ/zOPz8IZiRS
sx5AELcnvPvJ4j8qa1tSJVEaQE9wGdwyE+A/gyzrvwh2HEACZLDGTybwP0ll
ROdVqRlArC46XTzXzr9Zy1blTocfQIhm2f3Kwv0/rgeEGXkqIkCFI44yD+jd
P6zoNZ6KThVA2SA/OBpy8z+WYtdXrawhQE5hkm8HIPW/6OMNu/McI0DxiENQ
RIz7v2/yOoKhHCNAz33uBtxKCMDQMpRWZLUgQL9VAc4VUgRAXl56eD8OH0BW
4Qdilc/3P1jmGVJmwx5ASeCYHs/c+L+LFqju+I4gQPFqOngtczm/cZ1uF6z5
IkDt2iXDg6zIv1K5hp1ZCBVA3aB52GHq2L86bR3UnkQfQEedzcXKleS/YjiM
BY5fEEDzj/V4eOjpvxnW4QpS7BxAqX4RMJN45b8Aa7QIvaUiQDcUMuYi59q/
GJb+gd0gGkAPCk5P
      "]]}, {}}, {}, {}, {{}, {}}, {{}, {}}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{True, True}, {True, True}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{-10.736023811174835`, 
   12.377516513023526`}, {-6.597155331921314, 10.249331293172}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.731253296350259*^9},ExpressionUUID->"ce5e992b-aea4-45f6-b2b5-\
6694dc2dea1f"]
}, Open  ]]
},
WindowSize->{1659, 1773},
WindowMargins->{{345, Automatic}, {Automatic, 32}},
FrontEndVersion->"11.1 for Linux x86 (64-bit) (April 18, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 300, 8, 79, "Input", "ExpressionUUID" -> \
"ea5b0ccb-62f1-4c2d-a73a-bb782a62c9ea"],
Cell[CellGroupData[{
Cell[883, 32, 1772, 52, 107, "Input", "ExpressionUUID" -> \
"aa52163b-4dc9-4bdd-8811-38f1a07c3ede"],
Cell[2658, 86, 5608, 108, 245, "Output", "ExpressionUUID" -> \
"750f0683-b120-4601-9c4b-aebdcc461329"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8303, 199, 222, 5, 35, "Input", "ExpressionUUID" -> \
"af7540c1-7dbb-4a43-9ef6-fa10e64e8b63"],
Cell[8528, 206, 9591, 197, 69, "Output", "ExpressionUUID" -> \
"0434e3a9-6302-4550-8974-a1c620a13c52"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18156, 408, 464, 13, 35, "Input", "ExpressionUUID" -> \
"9008b103-00bc-4dc2-b7fc-49805baba184"],
Cell[18623, 423, 6058, 118, 245, "Output", "ExpressionUUID" -> \
"ce5e992b-aea4-45f6-b2b5-6694dc2dea1f"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

