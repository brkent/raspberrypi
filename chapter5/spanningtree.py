import math
import numpy as np
from random import random
from mst_clustering import MSTClustering
import matplotlib.pyplot as plt

"""
Science and Computing with Raspberry Pi
Brian R. Kent
"""

# Random points
minimum = 0
maximum = 10

cluster = []
for i in range(100):
    x = minimum + (maximum - minimum) * random()
    y = minimum + (maximum - minimum) * random()
    cluster.append([x,y])
    
model = MSTClustering(cutoff_scale=2, approximate=False)
labels = model.fit_predict(cluster) 


fig = plt.figure(figsize=(10,10), facecolor='white')
ax = fig.add_subplot(111)
X = model.X_fit_
segments = model.get_graph_segments(full_graph=True)
ax.plot(segments[0],segments[1], '-k', zorder=1, lw=1)
ax.scatter(X[:, 0], X[:, 1])

ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_title('Minimum Spanning Tree')

plt.savefig('minspantree.png')

plt.show()